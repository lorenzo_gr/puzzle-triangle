﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Collections;

namespace PuzzleTriangle
{
    class Program
    {
        static void Main(string[] args)
        {
            //reading from static file
            var AppRoot = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            var textFromFile = File.ReadAllText(AppRoot + "\\triangle.txt");

            //reading file from web
            //var url = "http://www.yodlecareers.com/puzzles/triangle.txt";
            //var textFromFile = (new WebClient()).DownloadString(url);

            int[][] numeri = new int[100][];
            using (StringReader reader = new StringReader(textFromFile))
            {
                string line;
                int linenumber = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    numeri[linenumber] = Array.ConvertAll(line.TrimEnd().Split(' '), int.Parse);
                    linenumber++;
                }
            }

            for (int row = 99; row >= 0; row--)
            {
                for (int col = 0; col < (numeri[row].Length-1); col++)
                {
                    numeri[row - 1][col] += Math.Max(numeri[row][col],numeri[row][col + 1] );
                }
            }
            Console.WriteLine(numeri[0][0]);
            Console.ReadKey();
        }
    }
}
