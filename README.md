# Solution for the Puzzle Triangle test code #

C# application for the  Puzzle Triangle problem by [Jodle](http://www.yodlecareers.com/puzzles/triangle.html)

### Start ###

Just loading data in a bidimensional array 

### Processing ###

Starting from the bottom to the top, find the greatest number of adjacent number and add it to the above number  

### Finally ###

Print the new value of the top number stored in [0][0] position 